# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
import os
basedir = os.path.abspath(os.path.dirname(__file__))

DEFAULTS = {
'MAIN_LOOP_SLEEP': 10,
'MAX_FAIL': 6,
'TRY_RESTART_DELAY': 5*60,
'THREAD_LOOP_SLEEP': 20,
'MAX_GET_TORRENTS_COUNTER':15,
}

class Config:
    TTD_DATABASE_URL = os.environ.get(u'TTD_DATABASE_URL')
    TORRENT_PATH = os.environ.get(u'TORRENT_PATH')

    MAIN_LOOP_SLEEP = float(os.environ.get(u'MAIN_LOOP_SLEEP') \
            or DEFAULTS['MAIN_LOOP_SLEEP'])

    MAX_FAIL = int(os.environ.get(u'MAX_FAIL') \
            or DEFAULTS['MAX_FAIL'])

    TRY_RESTART_DELAY = int(os.environ.get(u'TRY_RESTART_DELAY') \
            or DEFAULTS['TRY_RESTART_DELAY'])

    THREAD_LOOP_SLEEP = int(os.environ.get(u'THREAD_LOOP_SLEEP') \
            or DEFAULTS['THREAD_LOOP_SLEEP'])

    MAX_GET_TORRENTS_COUNTER = int(os.environ.get(u'MAX_GET_TORRENTS_COUNTER') \
            or DEFAULTS['MAX_GET_TORRENTS_COUNTER'])


    @staticmethod
    def init_app(app):
        pass

config={
    'default':Config
        }
