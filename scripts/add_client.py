import sys
sys.path.append('.')


import os
basedir = os.path.abspath(os.path.dirname(__file__))
import argparse

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from ttdmodel import db
from ttdmodel import MonitoringRequest, MonitoringStatus, Torrent, Client
from time import sleep
from pprint import pprint

db.init_app( os.environ.get('TTD_DATABASE_URL'))



def main(argv):
    parser = argparse.ArgumentParser(description='add client in torrent monitor')

    parser.add_argument('ipt', type=str)
    parser.add_argument('login', type=str)
    parser.add_argument('password', type=str)
    args = parser.parse_args()
    if not args.ipt:
        print "No IP tinc address"
        return 1
    if not args.login:
        print "No login"
        return 1
    if not args.password:
        print "No password"
        return 1

    client = db.Session.query(Client).filter_by(ipt=args.ipt).first()
    if client:
        print "Client already exists ( {}:{}@{} )".format(client.login, client.password, client.ipt)
        val = raw_input('Modify client (y/n)')
        if val == 'y':
            client.ipt = args.ipt
            client.login = args.login
            client.password = args.password
            print "client updated"
        else:
            print "No modification done on current client"

    else:
        print "Adding new client"
        clt = Client(ipt=args.ipt, login=args.login, password= args.password,
                    client_type = u'cinema')
        db.Session.add(clt)
    db.Session.commit()


if __name__ == "__main__":
   sys.exit(main(sys.argv))
